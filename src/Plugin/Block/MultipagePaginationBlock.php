<?php

namespace Drupal\sector_multipage\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Sector Multipage pagination' block.
 *
 * Block content is hidden until revealed by multipagePager in multipage.js.
 *
 * @Block(
 *   id = "sector_multipage_pagination_block",
 *   admin_label = @Translation("Sector Multipage Pagination block"),
 *   category = @Translation("Sector"),
 * )
 */
class MultipagePaginationBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<nav role="navigation">
  <ul aria-label="Pagination" class="multipage-pagination pagination--large-arrows list-unstyled hidden">
    <li class="pagination__previous"><a href="#" id="multipage-previous">Previous</a></li>
    <li class="pagination__next"><a href="#" id="multipage-next">Next</a></li>
  </ul>
</nav>',
      '#attached' => [
        'library' => 'sector_multipage/sector_multipage',
      ],
    ];
  }

}
