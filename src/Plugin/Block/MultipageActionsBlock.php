<?php

namespace Drupal\sector_multipage\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Sector Multipage actions' block.
 *
 * Note that we default to paged mode (not ?full).
 *
 * @Block(
 *   id = "sector_multipage_actions_block",
 *   admin_label = @Translation("Sector Multipage Actions block"),
 *   category = @Translation("Sector"),
 * )
 */
class MultipageActionsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '
      <div class="button-wrapper">
        <a role="button" class="btn btn-light multipage-disable-pagination" href="?full" title="Disable pagination">View all on one page</a>
      </div>

      <div class="button-wrapper">
        <a role="button" class="btn btn-light multipage-enable-pagination" href="?paged" title="Enable pagination">Show content paged</a>
      </div>

      <div class="button-wrapper">
        <a class="btn btn-default action--multipage-print js-multipage-print-page" href="#" rel="nofollow">Print this publication</a>
      </div>',
      '#allowed_tags' => ['a', 'button', 'div'],
      '#attached' => [
        'library' => 'sector_multipage/sector_multipage',
      ],
    ];
  }

}
