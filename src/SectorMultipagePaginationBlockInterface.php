<?php

namespace Drupal\sector_multipage;

/**
 * Provides constants used by this module.
 */
interface SectorMultipagePaginationBlockInterface {

  /**
   * The field name of the pagination block.
   *
   * @var string
   */
  public const FIELD_NAME = 'field_sectormultipagepaginationblock';

  /**
   * The machine name of the block loaded in the extra field.
   *
   * @var string
   */
  public const BLOCK_NAME = 'sectormultipagepaginationblock';

  /**
   * List of content types that will have the extra field.
   *
   * @var string[]
   */
  public const CONTENT_TYPES = [
    'resource',
  ];

}
