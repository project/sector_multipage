## Introduction

Provides in-page navigation between sections of long HTML documents after they have been chunked by the 'chunker' module. Typically used with Sector distribution Resource nodes.

Provides two blocks:
* Sector Multipage Pagination block, required to appear on content type default display (attaches `multipage.js`).
* Sector Multipage Actions block, to show in sidebar, offering buttons to switch between full and paged display.

## Requirements

This modules requires the following modules:
* [Sector distribution](https://drupal.org/project/sector)
* Chunker

## Installation

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.

## Configuration

Configure the Sector Multipage Pagination block to display, usually as the last block in the Content region, e.g. via context reaction to content type is Resource, or via Block layout when Content type is Resource.

Configure the Sector Multipage Actions block to display, usually in the sidebar, for the same conditions as the Sector Multipage Pagination block above.
