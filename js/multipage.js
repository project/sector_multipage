/**
 * Enable full page view or in-page pagination of sections.
 */
(function ($) {
  'use strict';

  /**
   * A pager for the multipage document.
   */
  Drupal.behaviors.multipagePager = {
    attach: function (context, settings) {

      var pagingIsEnabled = true;
      // Check number of sections.
      var numSections = $('.chunker-section h2').length;
      // Look for ?full parameter in URL.
      let params = new URLSearchParams(location.search);
      if (params.has('full')) {
        pagingIsEnabled = false;
        logger('param=full, so pagingIsEnabled is ' + pagingIsEnabled);
        if (numSections >= 2) {
          $('.multipage-enable-pagination').removeClass('disabled').removeAttr('aria-disabled');
        }
        else {
          $('.multipage-enable-pagination').addClass('disabled').attr('aria-disabled', 'true');
        }
        $('.multipage-disable-pagination').addClass('disabled').attr('aria-disabled', 'true');
      }
      else if (numSections >= 2) {
        pagingIsEnabled = true;
        logger('param!=full, so pagingIsEnabled is ' + pagingIsEnabled);
        $('.multipage-enable-pagination').addClass('disabled').attr('aria-disabled', 'true');
        $('.multipage-disable-pagination').removeClass('disabled').removeAttr('aria-disabled');
        $('.block-sector-multipage-pagination-block').removeClass('hidden');
      }
      else {
        pagingIsEnabled = false;
        logger('param!=full and numSections=' + numSections + ', so pagingIsEnabled is ' + pagingIsEnabled);
        $('.multipage-enable-pagination').addClass('disabled').attr('aria-disabled', 'true');
        $('.multipage-disable-pagination').removeClass('disabled').removeAttr('aria-disabled');
      }

      function multipageLocationHashChange() {
        var hash = location.hash;
        logger("multipageLocationHashChange: hash=" + hash);

        // If hash is in a section, make sure that section is visible.
        // Are we in paged mode via pagingIsEnabled? If not paged, ignore.
        if (pagingIsEnabled) {
          // Do we have a hit of a fragment inside a chunker section?
          $('div.chunker-section ' + hash).each(function () {
            logger('match for hash inside chunker-section');
            // Get element by id, find parent section, make it visible.
            $(hash).parents('div.chunker-section').show().siblings('div.chunker-section').hide();
            logger('section to show is ' + $(hash).html());
          });
        }

        // Add fragment to href in action buttons so we keep current viewing place.
        $('.block-sector-multipage-actions-block a[href*="?full"]').attr('href', '?full' + hash);
        $('.block-sector-multipage-actions-block a[href*="?paged"]').attr('href', '?paged' + hash);
      }

      window.addEventListener("hashchange", multipageLocationHashChange, false);

      // On page load, do we have a URL fragment?
      // e.g. arriving from bookmark, or refresh existing page?
      if (location.hash) {
        logger('detected hash in url, triggering local hashchange event')
        multipageLocationHashChange();
      }

      if (($(".prose").length > 0) && pagingIsEnabled) {

        // Hide all sections other than first.
        if(window.location.hash) {
          logger('window.location.hash', window.location.hash)
        }
        else {
          logger('no url fragment, start on section 0, hide others');
          $(".chunker-section").each(function (e) {
            if (e !== 0) {
              $(this).hide();
            }
          });
        }

        // Unhide pager controls
        $("ul.pagination").removeClass('hidden');

        $("#multipage-previous").click(function () {
          // Show the previous section, hide the current one.
          var previousItem = null;
          if ($(".chunker-section:visible").prev().length !== 0) {
            previousItem = $(".chunker-section:visible").prev();
            previousItem.show().next().hide();
          }
          else {
            // Wrap around to end.
            previousItem = $(".chunker-section:last");
            $(".chunker-section:visible").hide();
            previousItem.show();
          }
          scrollTop();

          return false;
        });

        $("#multipage-next").click(function () {
          // Show the next section, hide the current one.
          var nextItem = null;
          if ($(".chunker-section:visible").next().length !== 0) {
            nextItem = $(".chunker-section:visible").next();
            nextItem.show().prev().hide();
          }
          else {
            // Wrap around to start.
            $(".chunker-section:visible").hide();
            nextItem = $(".chunker-section:first");
            nextItem.show();
          }
          scrollTop();

          return false;
        });
      }

      function scrollTop() {
        // Make the correct focus point in the HTML.
        logger('scrolling to top...');
        $("html, body").animate({scrollTop: $(".chunker-section:visible").offset().top}, "fast");
      }

      function logger(message) {
        const prefix = 'sector_multipage: ';
        const debug = false;

        if (debug) {
          if (typeof message === 'object') {
            console.log(prefix, message);
          }
          else {
            console.log(prefix + message);
          }
        }
      }

      // Print this publication button.
      $(once('sector-multipage-print', '.js-multipage-print-page', context)).click(function (event) {
        event.preventDefault();

        // Show hidden sections so they are visible when printed.
        const $hiddenSections = $('.chunker-section:hidden');
        $hiddenSections.show();

        // Open print dialog.
        window.print();

        // Retore the hidden state of the section shown for print.
        $hiddenSections.hide();
      });
    }
  };

})(jQuery);
